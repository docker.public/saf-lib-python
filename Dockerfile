FROM eef_ubuntu:trusty
MAINTAINER Miguel Neyra <mneyra@americatel.com.pe>

COPY files/Python-2.6.6.tgz /tmp/p266.tgz

RUN cd /tmp && tar zxvf p266.tgz \
		&& cd /tmp/Python-2.6.6 && ./configure \
		&& cd /tmp/Python-2.6.6 && make \
		&& cd /tmp/Python-2.6.6 && make install